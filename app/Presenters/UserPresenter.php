<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use App\Model\Uzivatel;
use App\Model\Organizacia;

class UserPresenter extends BasePresenter
{  
    public function __construct(Nette\Database\Context $database, Uzivatel $uzivatel) {
        parent::__construct($database, $uzivatel);
    }
    
    public function renderDefault() //defaultny vypis nasich rfidiek
    {
        
    }
    
//----------------------------------------------------------------------------------------------------   
    protected function createComponentUserFormPasswordEdit() : Nette\Application\UI\Form
    {
        $this->uzivatel->loadUserFromDatabase( $this->getUser()->id );
        $form = new Form;
        
        $form->addPassword('password', '-Nove heslo:-')
            ->addCondition(Form::FILLED)
            ->addRule(Form::MIN_LENGTH, '-Položka %label musí obsahovat min. %d znaků-', 6)
            ->addRule(Form::MAX_LENGTH, '-Položka %label může obsahovat max. %d znaků-', 255);;
        
        $form->addPassword('password_again', '-Heslo (znovu):-')
            ->addConditionOn($form['password'], Form::FILLED)
            ->setRequired( "Vyplnte heslo" )
            ->addRule(Form::EQUAL, "-Hesla se musí shodovat!-", $form["password"])
            ->addRule(Form::MIN_LENGTH, '-Položka %label musí obsahovat min. %d znaků-', 6)
            ->addRule(Form::MAX_LENGTH, '-Položka %label může obsahovat max. %d znaků-', 255);
        
        $form->addSubmit('send', "Ulozit" );
        $form->onSuccess[] = [$this, 'PasswordFormSubmitted']; //spracovanie formulara bude mat na starosti funckia tejto triedy s nazvom: pridajRFIDFormSubmitted
        
        return $form;
    }
    
    public function PasswordFormSubmitted( $form, $values ) : void
    {
        try {
            $this->uzivatel->savePasswordToDatabase($values->password);
            $this->flashMessage("-Zmeny sa podarili-", "alert alert-success" );
        } catch (\Exception $ex) {
            $this->flashMessage("-Nepodarilo sa zmenit heslo-", "alert alert-warning" );
        }    
    }
//----------------------------------------------------------------------------------------------------    

//----------------------------------------------------------------------------------------------------   
protected function createComponentUserFormHodinovaTaxa() : Nette\Application\UI\Form
{
    $this->uzivatel->loadUserFromDatabase( $this->getUser()->id );
    $form = new Form;
    
    $form->addText('taxa', 'Hodinova sadzba:')
        ->setDefaultValue( $this->uzivatel->getTaxa() )
        ->addCondition(Form::FILLED)
        ->addFilter(function ($value) {
            return str_replace(',', '.', $value); // zmenime ciary na bodky v cislach
        })
        ->addRule(Form::MIN, 'Hodnota musi byt cislo a aspon s hodnotou 0 ', 0) ;
        
    $form->addSubmit('send', "Ulozit" );
    $form->onSuccess[] = [$this, 'HodinovaTaxaFormSubmitted']; //spracovanie formulara bude mat na starosti funckia tejto triedy s nazvom: pridajRFIDFormSubmitted
    
    return $form;
}
public function HodinovaTaxaFormSubmitted( $form, $values ) : void
    {
        try {
            $this->uzivatel->saveTaxaToDatabase($values->taxa);
            $this->flashMessage("-Zmeny sa podarili-", "alert alert-success" );
        } catch (\Exception $ex) {
            $this->flashMessage("-Nepodarilo sa zmenit heslo-", "alert alert-warning" );
        }    
    }
//----------------------------------------------------------------------------------------------------    
    
}//end class