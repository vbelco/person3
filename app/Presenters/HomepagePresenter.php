<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use App\Model\Uzivatel;
use App\Model\Dochadzka; //pripojime triedu 

class HomepagePresenter extends BasePresenter
{
     /** @var Dochadzka */
    private $dochadzka; //trieda dochadzky
    
    public function renderDefault()
    {
        //uvitacia stranka
        
    }
    
    protected function createComponentPrehladForm() {
        $form = new Form;
        
        $form->addText('datum_od', "Od:" )
                ->setRequired( "Uvedte zaciatocny datum" );
        
        $form->addText('datum_do',  "Do:" )
                ->setRequired( "Uvedte datum do." );
        
        $form->addSubmit('send', "Zobraz" );
        $form->onSuccess[] = [$this, 'prehladFormSubmitted']; //spracovanie formulara bude mat na starosti funckia tejto triedy s nazvom: pridajRFIDFormSubmitted
        
        return $form;
    }
    
    public function prehladFormSubmitted( $form, $values ) {
        $this->dochadzka = new Dochadzka($this->database);
        $this->uzivatel->loadUserFromDatabase($this->getUser()->id); //znova nacitt uzivatela? zaujimave ze to treba
        
        //uvodne nastavovacky
        $this->dochadzka->setUserId( $this->uzivatel->getUserId() ); //ziskame user_id uzivatela ktoremu patri tento clovek
        $this->dochadzka->setZaokruhlovanieFromDatabase();
        $this->dochadzka->setToleranciaFromDatabase(); //nastavenie predvolenej tolerancie neskorzch prichodov
        $this->dochadzka->setVypisRealCasovFromDatabase(); //nastavenie  ci sa maju vypisvat aj realne, hrube casy popri zaokruhlenych
        $od = $values['datum_od'];
        $do = $values['datum_do'];
        
        $pole_dochadzka_zaokruhlena = array();
        $pole_raw_dochadzka = array();
        $pole_celkovy_cas_dochadzky = array("hodiny" => 0, "minuty" => 0);
        
        $this->dochadzka->generuj_raw_dochadzku_cloveka($this->getUser()->id, $od, $do); //naplni objekt udajmi o raw dochaddzke cloveka
        $this->dochadzka->generuj_zaokruhlenu_dochadzku_cloveka($this->getUser()->id, $od, $do);//naplni objekt udajmi o dochaddzke cloveka
        
        $pole_dochadzka_zaokruhlena= $this->dochadzka->getPoleDochadzky(); //zaokruhli dochadzku podla nastaveneho zaokruhlovania
        $pole_raw_dochadzka = $this->dochadzka->getPoleRawDochadzky();
        $this->dochadzka->sumarizuj(); //spocitame celkovy cas za obdobie
        $temp_celkovy_cas = $this->dochadzka->getCelkovyCasDochadzky(); //ziskame hodnotu v DateInterval objekte
        if ( isset($temp_celkovy_cas) ){ //ak mame nejaku dochadzku
            $temp_hodiny = ($temp_celkovy_cas->days*24) + ($temp_celkovy_cas->h); //pocet hodin
            $temp_minuty = $temp_celkovy_cas->i; //pocet minut
            $pole_celkovy_cas_dochadzky["hodiny"] = $temp_hodiny;
            $pole_celkovy_cas_dochadzky["minuty"] = $temp_minuty; 
            $temp_zarobok = (($temp_hodiny*60) + $temp_minuty) * ($this->uzivatel->getTaxa() / 60 );
        }
        
        $this->template->meno_osoby = $this->uzivatel->getName();
        $this->template->posts_zaokruhlena_dochadzka = $pole_dochadzka_zaokruhlena;
        $this->template->posts_raw_dochadzka = $pole_raw_dochadzka;
        $this->template->celkovy_cas_dochadzky = $pole_celkovy_cas_dochadzky;
        $this->template->vypisovat_realne_casy = $this->dochadzka->getVypisRealCasov(); //priznak, ze ci budemem vypisovat aj realne casy
        $this->template->celkova_suma_zarobku = $temp_zarobok;
        $this->template->hodinova_taxa = $this->uzivatel->getTaxa();
    }

    
}
