<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\Passwords;
use App\UserManager;


class SignPresenter extends BasePresenter
{
    
    protected function createComponentSignInForm()
    {
        $form = new Form;
        $form->addText('meno', '-Prihlasovacie meno-:')
            ->setRequired( '-Vyplnte prihlasovacie meno-' );

        $form->addPassword('password', "Heslo:" )
            ->setRequired( "Vyplnit heslo" )
            ->addRule(Form::MIN_LENGTH, '-Položka %label musí obsahovat min. %d znaků-', 6)
            ->addRule(Form::MAX_LENGTH, '-Položka %label může obsahovat max. %d znaků-', 255);

        $form->addSubmit('send', "Login" );

        $form->onSuccess[] = [$this, 'signInFormSucceeded'];
        return $form;
    }
    
    public function signInFormSucceeded($form, $values)
    {
        try {
            $this->getUser()->login($values->meno, $values->password);
            $this->redirect('Homepage:');

        } catch (Nette\Security\AuthenticationException $e) {
            $form->addError( "Zle heslo");
        }
    }

    public function actionOut()
    {
        $this->getUser()->logout();
        $this->flashMessage( "Odhlasit:", "alert alert-success" );
        $this->redirect('Homepage:');
    }
}