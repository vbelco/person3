<?php
/*
trieda reprezentujuca uzivatela
  */

namespace App\Model;

use Nette;

class Uzivatel
{
    /**
     * @var Nette\Database\Context
     */
    private $database;

    /**
     * @var Nette\Security
     */
    private $passwords;
    
    private $id; //identifikcne uzivatela
    private $name; //meno uzivatela
    private $user_id; //id uzivatela ktoremu je tento clovek priradeny
    private $oddelenie_id; //do ktoreho oddelenia tento clovek patri
    private $login;  // akz ma login na kontrolu svojej dochadzky
    private $hodinova_taxa; //hodinova taxa cloveka
    
    
    function __construct(Nette\Database\Context $database,  Nette\Security\Passwords $passwords)
    {
        $this->database = $database;
        $this->passwords = $passwords;
    }
    
    public function getId()  { return $this->id; }
    public function getName() { return $this->name; }
    public function getUserId() { return $this->user_id; }
    public function getTaxa() :float 
    { 
        return $this->hodinova_taxa; 
    }
    
    public function setName($name) {$this->name = $name; }
        
    public function loadUserFromDatabase ($id){
        try {
            $row = $this->database->table('people')->get($id);
            if ($row){ //je taka osoba v databaze
                $this->id = $row->id;
                $this->name = $row->meno;
                $this->user_id = $row->users_id;
                $this->oddelenie_id = $row->oddelenie_id;
                $this->login = $row->login;
                $this->hodinova_taxa = $row->hodinova_taxa;
            }
        } catch (\Exception $ex) {
            throw new \ErrorException;
        }  
    }
    
    public function savePasswordToDatabase ($password) {
        try{
            $this->database->table('people')
                ->where('id', $this->id) // must be called before update()
                ->update([ 'password' => $this->passwords->hash($password)  ]);
        } catch ( Nette\Database\ConnectionException $e ){
            throw new \ErrorException;
        }
    }

    public function saveTaxaToDatabase($taxa) :void
    {
        try{
            $this->database->table('people')
                ->where('id', $this->id) // must be called before update()
                ->update([ 'hodinova_taxa' => $taxa  ]);         
        } catch (Nette\Database\ConnectionException $e ){
            throw new \ErrorException;
        }
    }
    
}
