<?php

use Nette\Security as NS;

class MojPrihlasovac implements NS\IAuthenticator
{
    public $database;
    private $passwords;

    function __construct(Nette\Database\Context $database, Nette\Security\Passwords $passwords)
    {
        $this->database = $database;
        $this->passwords = $passwords;
    }

    function authenticate(array $credentials) : Nette\Security\IIdentity
    {
        [$meno, $password] = $credentials;

        $row = $this->database->table('people')
			->where('login = ?', $meno)
			->fetch();

        if (!$row) {
            throw new Nette\Security\AuthenticationException('User not found.');
        }

        if (!$this->passwords->verify($password, $row->password)) {
			throw new Nette\Security\AuthenticationException('Invalid password.');
		}
		$arr = $row->toArray();
		unset($arr['password']);
        return new Nette\Security\Identity($row->id, '', $arr );
    }
}